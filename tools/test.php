<?php

/**
 -------------------------------------------------------------------------
 Config plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Config.

 Config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

require_once __DIR__ . '/../../../inc/includes.php';

$cfg = new PluginConfigVariable('config');

$s = 'STriNg';
$i = 1;
$f = 1.11;
$arr = array('a', 'b', 'c');
$aarr = array('a' => 1, 'b' => 2, 'c' => 3);
$obj = (object) $aarr;
$vals = array($s, $i, $f, $arr, $aarr, $obj);

foreach ($vals as $val) {

   $type = gettype($val);

   $tmp = $cfg->get($type, $val);
   var_dump($tmp);

   $cfg->set($type, $val);
   $tmp = $cfg->get($type);
   var_dump($tmp);

   $cfg->set($type, $val);

   $cfg->del($type);

   $tmp = $cfg->get($type);
   var_dump($tmp);
   $cfg->set($type, $val);

}

$cfg->delAll();

foreach ($vals as $val) {
   $tmp = $cfg->get($type);
   var_dump($tmp);
}
