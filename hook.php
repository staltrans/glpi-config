<?php
/*
 -------------------------------------------------------------------------
 config plugin for GLPI
 Copyright (C) 2017 by the config Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of config.

 config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_config_install() {
   global $DB;
   $storage = 'glpi_plugin_config_variables';
   $create_storage = "
      CREATE TABLE `$storage` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `entities_id` int(11) NOT NULL DEFAULT '0',
        `is_recursive` tinyint(1) NOT NULL DEFAULT '0',
        `plugin` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
        `date_mod` timestamp NULL DEFAULT NULL,
        `date_creation` timestamp NULL DEFAULT NULL,
        `value` blob NOT NULL DEFAULT '',
      PRIMARY KEY (`id`),
      UNIQUE `name_plugin_entities` (`name`, `plugin`, `entities_id`),
      KEY `name` (`name`),
      KEY `entities_id` (`entities_id`),
      KEY `plugin` (`plugin`),
      KEY `date_mod` (`date_mod`),
      KEY `date_creation` (`date_creation`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
   if (!$DB->tableExists($storage)) {
      $DB->queryOrDie($create_storage);
   } else {
      if ($DB->fieldExists($storage, 'variable')) {
         $result = $DB->query("SELECT * FROM `$storage`");
         $variables = [];
         while ($value = $DB->fetch_assoc($result)) {
            $variables[] = $value;
         }
         $DB->queryOrDie("DROP TABLE `$storage`");
         $DB->queryOrDie($create_storage);
         foreach ($variables as $var) {
            $now = date('Y-m-d H:i:s');
            $value = $DB->escape($var['value']);
            $insert = "
               INSERT INTO `$storage`
                  (`name`, `plugin`, `date_mod`, `date_creation`, `value`)
               VALUES ('{$var['variable']}', '{$var['plugin']}', '$now', '$now', '$value')";
            $DB->queryOrDie($insert);
         }
      } else if (!$DB->fieldExists($storage, 'is_recursive')) {
         $alter = "ALTER TABLE `$storage` ADD `is_recursive` tinyint(1) NOT NULL DEFAULT '0'";
         $DB->queryOrDie($alter);
      }
   }
   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_config_uninstall() {
   global $DB;
   $storage = 'glpi_plugin_config_variables';
   if ($DB->tableExists($storage)) {
      $query = "DROP TABLE `$storage`";
      $DB->queryOrDie($query);
   }
   return true;
}
