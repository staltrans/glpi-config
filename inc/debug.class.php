<?php

/**
 -------------------------------------------------------------------------
 Config plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Config.

 Config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginConfigDebug extends PluginConfigVariable {

   const VAR_DEBUG = 'debug';

   //static $rightname      = 'plugin_config_debug';
   protected $displaylist = false;

   function defaultValue($variable) {
      switch ($variable) {
         case self::VAR_DEBUG:
            return 0;
         default:
            return false;
      }
   }

   function showFormMain() {
      $val = $this->defaultValue(self::VAR_DEBUG);
      if (!empty($this->fields['value'])) {
         $val = $this->fields['value'];
      }
      echo '<tr class="tab_bg_2">';
      echo '<td>' . PluginConfigTr::__('Отладка включена') . '</td>';
      echo '<td colspan="2">';
      echo '<input type="hidden" name="name" value="' . self::VAR_DEBUG . '">';
      echo '<input type="hidden" name="plugin" value="' .  static::PLUGIN . '">';
      Dropdown::showYesNo('value', $val);
      echo '</td>';
      echo '</tr>';
   }

   static function getDebugMenuLink() {
      global $CFG_GLPI;
      $img = Html::image(
         $CFG_GLPI['root_doc'] . '/plugins/config/img/icon-bug-20x18.png',
         ['alt' => PluginConfigTr::__('Отладка')]
      );
      return [$img => static::getFormURL()];
   }

   static function variable($name, $value) {
      $me = new static();
      if ($me->get(self::VAR_DEBUG)) {
         $msg = "\$$name =\n";
         $msg .= var_export($value, true) . "\n";
         Toolbox::logInFile($me::PLUGIN . '-debug', $msg, true);
      }
   }

}
