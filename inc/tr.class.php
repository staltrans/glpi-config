<?php

/**
 -------------------------------------------------------------------------
 Config plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Config.

 Config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * This class contains translates functions
 * For more info see
 * https://github.com/glpi-project/glpi/blob/9.2/bugfixes/inc/autoload.function.php
 */
class PluginConfigTr {

   const DOMAIN = 'config';

   static function __($str) {
      return __($str, static::DOMAIN);
   }

   static function __s($str) {
      return __s($str, static::DOMAIN);
   }

   static function _sx($ctx, $str) {
      return _sx($ctx, $str, static::DOMAIN);
   }

   static function _e($str) {
      return _e($str, static::DOMAIN);
   }

   static function _n($sing, $plural, $nb) {
      return _n($sing, $plural, $nb, static::DOMAIN);
   }

   static function _sn($sing, $plural, $nb) {
      return _sn($sing, $plural, $nb, static::DOMAIN);
   }

   static function _x($ctx, $str) {
      return _x($ctx, $str, static::DOMAIN);
   }

   static function _ex($ctx, $str) {
      return _ex($ctx, $str, static::DOMAIN);
   }

   static function _nx($ctx, $sing, $plural, $nb) {
      return _nx($ctx, $sing, $plural, $nb, static::DOMAIN);
   }

}
