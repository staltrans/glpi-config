<?php

/**
 -------------------------------------------------------------------------
 Config plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Config.

 Config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginConfigVariable extends CommonDBTM {

   // Plugin name
   protected $plugin;

   public $dohistory         = true;
   public $history_blacklist = ['date_mod'];

   static $rightname         = 'plugin_config_variable';

   const PLUGIN = 'config';

   /**
    * Get translated name for item
    *
    * @param $nb int items numbers
    *
    * @return string
    */
   static function getTypeName($nb = 0) {
      return PluginConfigTr::_n('Переменная конфигурации', 'Переменные конфигурации', $nb);
   }

   /**
    * Return the table used to store this object
    *
    * @return string
    */
   static function getTable($classname = NULL) {
      if (empty($classname)) {
         $classname = 'PluginConfigVariable';
      }
      return parent::getTable($classname);
   }

   /**
    * Sets the value of a variable
    *
    * @param string $variable the variable name
    * @param mixed $value value of a variable
    *
    * @return bool if variable set
    */
   function set($variable, $value) {
      $criterias = $this->getCriterias($variable);
      $ok = $this->getFromDBByCrit($criterias);
      if ($ok) {
         $this->fields['value'] = $value;
         return $this->update($this->fields);
      } else {
         $criteris['value'] = $value;
         return $this->add($criterias);
      }
   }

   /**
    * Gets the value of a variable
    *
    * @param string $variable Variable name
    * @param mixed $default The default value of the veriable
    *
    * @return mixed
    */
   function get($variable) {
      $ok = $this->getFromDBByCrit($this->getCriterias($variable));
      if ($ok) {
         return $this->fields['value'];
      } else {
         return $this->defaultValue($variable);
      }
   }

   /**
    * For child classes
    * Define default value for variable
    *
    * @param $variable name of the variable
    *
    * @return mixed if default value exists else false
    */
   function defaultValue($variable) {
      return false;
   }

   /**
    * Delete variable
    *
    * @param string $variable Variable name
    *
    * @return bool, true if variable deletet in database
    */
   function del($variable) {
      return $this->deleteByCriteria($this->getCriterias($variable));
   }

   /**
    * Delete all variables
    *
    * @return bool, true if all variables deletet in database
    */
   function delAll() {
      return $this->deleteByCriteria(['plugin' => static::PLUGIN]);
   }

   function prepareInput($input) {
      global $DB;
      if (!empty($input['value'])) {
         $input['value'] = $DB->escape($this->encode($input['value']));
      }
      return $input;
   }

   /**
    * Prepare input datas for adding the item
    *
    * @param array $input datas used to add the item
    *
    * @return array the modified $input array
    */
   function prepareInputForAdd($input) {
      return $this->prepareInput($input);
   }

   /**
    * Prepare input datas for updating the item
    *
    * @param array $input data used to update the item
    *
    * @return array the modified $input array
    */
   function prepareInputForUpdate($input) {
      return $this->prepareInput($input);
   }

   /**
    * Actions done at the end of the getFromDB function
    *
    * @return void
    */
   function post_getFromDB() {
      $this->fields['value'] = $this->decode($this->fields['value']);
   }

   /**
    * Actions done before the UPDATE of the item in the database
    *
    * @return void
    */
   function pre_updateInDB() {
      if (isset($this->oldvalues['value'])) {
         $this->oldvalues['value'] = $this->encode($this->oldvalues['value']);
      }
   }

   function rawSearchOptions() {

      global $CFG_GLPI;

      $tab = [];

      $tab[] = [
         'id'                 => 'common',
         'name'               => __('Characteristics')
      ];

      $i = 0;

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'id',
         'name'               => PluginConfigTr::__('ID'),
         'massiveaction'      => false,
         'datatype'           => 'number'
      ];

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'name',
         'name'               => PluginConfigTr::__('Переменная'),
         'datatype'           => 'itemlink',
         'massiveaction'      => false
      ];

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'plugin',
         'name'               => PluginConfigTr::__('Плагин'),
         'datatype'           => 'string',
         'massiveaction'      => false
      ];

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'value',
         'name'               => PluginConfigTr::__('Значение переменной'),
         'datatype'           => 'blob',
         'massiveaction'      => false
      ];

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'date_mod',
         'name'               => PluginConfigTr::__('Дата обновления'),
         'datatype'           => 'datetime',
         'massiveaction'      => false
      ];

      $tab[] = [
         'id'                 => $i++,
         'table'              => $this->getTable(),
         'field'              => 'date_creation',
         'name'               => PluginConfigTr::__('Дата создания'),
         'datatype'           => 'datetime',
         'massiveaction'      => false
      ];

      return $tab;

   }

   function showForm($ID, $options = []) {
      $this->initForm($ID, $options);
      $this->showFormHeader($options);
      $this->showFormMain();
      $this->showFormButtons($options);
      return true;
   }

   function showFormMain() {
      echo '<tr>';
      echo '<td>' . PluginConfigTr::__('Переменная') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'name', ['option' => 'readonly']);
      echo '</td>';
      echo '<td rowspan="2">' . PluginConfigTr::__('Значение переменной') . '</td>';
      $value = var_export($this->fields['value'], true);
      echo '<td rowspan="2">';
      echo '<textarea cols="45" rows="4" readonly>' . $value . '</textarea>';
      echo '</td>';
      echo '</tr>';

      echo '<tr>';
      echo '<td>' . PluginConfigTr::__('Плагин') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'plugin', ['option' => 'readonly']);
      echo '</td>';
      echo '</tr>';
   }

   function save($post = []) {
      //Toolbox::logInFile('config', print_r($post, true), true);
      if (isset($post['add'])) {
         $this->check(-1, CREATE, $post);
         $this->add($post);
      } else if (isset($post['delete'])) {
         $this->check($post['id'], DELETE);
         $this->delete($post);
         $this->redirectToList();
      } else if (isset($post['restore'])) {
         $this->check($post['id'], PURGE);
         $this->restore($post);
         $this->redirectToList();
      } else if (isset($post['purge'])) {
         $this->check($post['id'], PURGE);
         $this->delete($post, 1);
         $this->redirectToList();
      } else if (isset($post['update'])) {
         $this->check($post['id'], UPDATE);
         $this->update($post);
      }
   }

   function defineTabs($options = []) {
      $ong = [];
      $this->addDefaultFormTab($ong);
      $this->addStandardTab('Log', $ong, $options);
      return $ong;
   }

   /**
    * Get active entity
    *
    * @return int, ID of the active entity
    */
   protected function getGLPIActiveEntity() {
      if (isset($_SESSION['glpiactive_entity'])) {
         return $_SESSION['glpiactive_entity'];
      }
      return 0;
   }

   /**
    * Get search criterias for variable
    *
    * @param $variable the variable name for search
    *
    * @return array of search criterias
    */
   protected function getCriterias($variable) {
      return [
         'name'        => $variable,
         'plugin'      => static::PLUGIN,
         'entities_id' => $this->getGLPIActiveEntity()
      ];
   }

   /**
    * Returns the JSON representation of a value
    *
    * @param $value mixed
    *
    * @return string containing the JSON representation of the supplied value
    */
   protected function encode($value) {
      return json_encode($value);
   }

   /**
    * Decodes a JSON string
    *
    * @param $json string, the JSON encoded string
    *
    * @return mixed
    */
   protected function decode($json) {
      return json_decode($json);
   }

}
