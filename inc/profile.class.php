<?php

/**
 -------------------------------------------------------------------------
 Config plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Config.

 Config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginConfigProfile extends Profile {

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == 'Profile') {
         return PluginConfigTr::__('Переменные конфигурации');
      }
      return '';
   }

   function getConfigRights() {
      return [
         [
            'itemtype' => 'PluginConfigVariable',
            'label'    => PluginConfigTr::__('Переменные'),
            'field'    => PluginConfigVariable::$rightname,
         ],
      ];
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {

      if ($item->getType() == 'Profile') {
         $pid = $item->getID();
         $me = new self();
         $me->showForm($pid);
      }
      return true;

   }

   function showForm($profiles_id = 0, $openform = true, $closeform = true) {

      if (!self::canView()) {
         return false;
      }

      $profile = new Profile();
      $canedit = Session::haveRightsOr(self::$rightname, [CREATE, UPDATE, PURGE]);

      echo '<div class="spaced">';

      if ($canedit && $openform) {
         echo '<form method="post" action="' . $profile->getFormURL() . '">';
      }

      $profile->getFromDB($profiles_id);

      $matrix_options = [
         'title'         => PluginConfigTr::__('Переменные конфигурации'),
         'canedit'       => $canedit,
         'default_class' => 'tab_bg_2'
      ];

      $profile->displayRightsChoiceMatrix($this->getConfigRights(), $matrix_options);

      if ($canedit && $closeform) {
         echo '<div class="center">';
         echo Html::hidden('id', ['value' => $profiles_id]);
         echo Html::submit(_sx('button', 'Save'), ['name' => 'update']);
         echo '</div>'; // .center
         Html::closeForm();
      }

      echo '</div>'; // .spaced
   }

}
