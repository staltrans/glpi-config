<?php
/*
 -------------------------------------------------------------------------
 config plugin for GLPI
 Copyright (C) 2017 by the config Development Team.

 https://bitbucket.org/staltrans/config
 -------------------------------------------------------------------------

 LICENSE

 This file is part of config.

 config is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 config is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with config. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_CONFIG_VERSION', '0.6');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_config() {
   global $PLUGIN_HOOKS;

   Plugin::registerClass('PluginConfigProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['csrf_compliant']['config'] = true;

   $PLUGIN_HOOKS['menu_toadd']['config'] = ['plugins' => 'PluginConfigVariable'];

}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_config() {
   return [
      'name'           => 'Config',
      'version'        => PLUGIN_CONFIG_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-config',
      'minGlpiVersion' => '9.3'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_config_check_prerequisites() {
   // Strict version check (could be less strict, or could allow various version)
   $plugin = new Plugin();
   if (version_compare(GLPI_VERSION, '9.3', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.3');
      } else {
         echo __('This plugin requires GLPI >= 9.3', 'config');
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_config_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'config');
   }
   return false;
}
